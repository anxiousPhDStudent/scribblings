---
layout: post
title:  "On toxicity and bullies who can get away with it"
date:   2018-05-22 10:11:12 -0300
---

# Toxicity in academia and a tale of two supervisors

I needed to work through some thoughts about a toxic person in academia, without putting myself at risk using my normal accounts to share things. 

## I'm very lucky

As a PhD student, I'm well aware that many students have a toxic supervisor and nowhere to reach out when they treat their students badly. I'm lucky enough to have two. Let's call one Allen - they're supportive, empowering, and offer critical and useful feedback. Let's call the other Brent: they're well intended, usually, and they have quite a bit of power, but they're a bully, and possibly unaware they are a bully. They might consider what they're doing "tough love". I've been aware of them as a worry for a long time, and afraid of them / treading carefully for a long time, avoiding them when I can in a bid to stay safe, but also worried the avoidance itself may be obvious. I have heard _of_ the inappropriate behaviours, from many people, some who experienced it firsthand, and some who witnessed it. Brent is a [missing stair](https://en.wikipedia.org/wiki/Missing_stair). 

## Things came to a (small) head, recently

Despite the fact that I was aware and wary of my missing stair, I didn't have any big run-ins with them - possibly partly because I took pains to avoid them, as much as was in my power. 

When a large amount of written work needed to be reviewed, however, there was little avoiding them, and their "school of hard knocks" approach to feedback - them thinking something like "you'll learn from this rude behaviour and thank me later".

There certainly was some stuff in there that might be called critical / useful feedback - and I certainly agree that my work needed to be improved. That's the whole purpose of an early draft, to show it to people who haven't been working on it, who don't have the whole picture in their head the same way you do, and who can see the gaps and the flaws from a few steps away. I've worked through iterations and heavy revisions of work many times - it's what happens.

Plain old insulting comments, and rude or pseudo-neutral objections to the content without any detail as to _what_ is wrong - these are not useful, nor a sign of a good collaborator. It's certainly possible to excuse some of the comments if you consider the fact that  text communication loses emotional nuance, and that negative comments are stronger than positive comments (quote here from [Muir K, Joinson A, Cotterill R, Dewdney N., "Linguistic Style Accommodation Shapes Impression Formation and Rapport in Computer-Mediated Communication"](https://doi.org/10.1177/0261927X17701327)):

> Previous studies showed negative evaluations had a greater impact on people than positive evaluations did. For example, if consumers read a set of online reviews containing equal amounts of positive and negative reviews, their attitude toward the reviewed topic was more negative ...

Nevertheless, it was absolutely clear that some of the communication was unreasonable and not useful.

## What do I want?

It's hella hard to think how to progress this work right now. It needs to be done and wrapped up. I want it out the door. I'm agonisingly time-strapped because I like to do too many things at once. I don't appreciate working with bullies, I _would not_ put up with this if it was a job. I would be looking for a new job. But I'm deeply embedded in my current degree and don't see a viable path to continue it elsewhere, nor do I wish to lose Allen, the good supervisor, nor do I wish burn bridges with Brent, the bad one. Not because I like them, but because they're powerful and could close some doors for me. NOT all the doors, but some of them. 

I want to graduate from THIS university, with this degree, using the work I have done so far.

## Reflections

If this was a community where I had authority, and I saw someone do this in public, I would tell them they needed to stop, and if they didn't, they would be asked to leave. Even if I didn't have authority, I'd probably speak up if it was safe to do so.

I've experienced similar scenarios where some people tried "tough love" (i.e. being rude and insulting in an attempt to make people do something the way they want). As I _was_ a person in authority I shook in my boots for a few minutes at the thought of confrontation, then bit the bullet, consulted with friends a bit to help phrase things well, and told the offender their behaviour wasn't acceptable. It didn't go down well with them - but they did cease without any other serious action needing to be taken. I also apologised on behalf of the community, to everyone who witnessed the behaviour. I got two sets of responses to the apology - one was "it's okay, I shrugged it off", and the other set: "This person made me unhappy / uncomfortable / angry" - which suggests that I did the right thing if I didn't want to lose half my community. 

It is helpful to me to have had this experience - seeing that there were positive and neutral responses to enforcing positive communication behaviour. Relating it to how this person behaves - if I wasn't directly under their power I know I would consider it unacceptable.



