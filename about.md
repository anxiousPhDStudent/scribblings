---
layout: page
title: About
permalink: /about/
---

Bullies shouldn't get away with it. We shouldn't be silent about their behaviour. 

But power imbalance makes it hard to act against, sometimes. 
